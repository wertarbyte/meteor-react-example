# Dies ist ein einfaches Beispiel für Meteor React mit Material UI und Reflux #

Angelehnt an das Meteor Beispiel;
https://www.meteor.com/tutorials/react/creating-an-app

Noch nicht überzeugt von Flux?
https://facebook.github.io/flux/docs/overview.html

## Genutzte Pakete: ##
* https://atmospherejs.com/fourquet/reflux
* https://atmospherejs.com/markoshust/material-ui