const {
  AppBar,
  IconButton,
  IconMenu,
  LeftNav,
  Paper,
  TextField,
  FlatButton,
  RaisedButton,
  MobileTearSheet,
  Card,
  Checkbox,
  List
  } = mui;
const { MenuItem } = mui.Menus;
const { NavigationMoreVert } = mui.SvgIcons;
const Styles = mui.Styles;
const Colors = Styles.Colors;

// App component - represents the whole app
App = React.createClass({
  // This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  // Loads items from the Tasks collection and puts them on this.data.tasks
  getMeteorData() {
    let query = {};

    if (this.state.hideCompleted) {
      // If hide completed is checked, filter tasks
      query = {checked: {$ne: true}};
    }

    return {
      tasks: Tasks.find(query, {sort: {createdAt: -1}}).fetch(),
      incompleteCount: Tasks.find({checked: {$ne: true}}).count(),
      currentUser: Meteor.user()
    };
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getInitialState() {
    return {
      open: false,
      hideCompleted: false
    };
  },

  getChildContext() {
    return {
      muiTheme: Styles.ThemeManager.getMuiTheme(Styles.LightRawTheme)
    };
  },

  handleToggle() {
    this.setState({open: !this.state.open});
  },

  toggleHideCompleted() {
    this.setState({
      hideCompleted: !this.state.hideCompleted
    });
  },

  getTasks() {
    return [
      {_id: 1, text: "This is task 1"},
      {_id: 2, text: "This is task 2"},
      {_id: 3, text: "This is task 3"}
    ];
  },

  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    var text = React.findDOMNode(this.refs.textInput).value.trim();

    Meteor.call("addTask", text);

    // Clear form
    React.findDOMNode(this.refs.textInput).value = "";
  },

  renderTasks() {
    // Get tasks from this.data.tasks
    return this.data.tasks.map((task) => {
      const currentUserId = this.data.currentUser && this.data.currentUser._id;
      const showPrivateButton = task.owner === currentUserId;

      return <Task
        key={task._id}
        task={task}
        showPrivateButton={showPrivateButton}/>;
    });
  },

  render() {
    return (
      <div className="app">
        <LeftNav
          docked={false}
          open={this.state.open}
          disableSwipeToOpen={false}
          swipeAreaWidth={100}
          onRequestChange={open => this.setState({open})}
          >
          <MenuItem linkButton={true} href="/home" primaryText="Home" index={1} onTouchTap={this.handleToggle}/>
          <MenuItem linkButton={true} href="/feature" primaryText="Feature" index={2} onTouchTap={this.handleToggle}/>
          <MenuItem linkButton={true} href="/contact" primaryText="Contact" index={3} onTouchTap={this.handleToggle}/>
        </LeftNav>
        <AppBar
          title="Home"
          onLeftIconButtonTouchTap={this.handleToggle}
          style={{backgroundColor: Colors.blue500}}
          iconElementRight={
            <IconMenu
              iconButtonElement={
                <IconButton>
                  <NavigationMoreVert />
                </IconButton>
              }
            >
              <MenuItem primaryText="Help" index={1} />
              <MenuItem primaryText="Sign out" index={2} />
            </IconMenu>
          }
          />

        <h1>Todo List ({this.data.incompleteCount})</h1>

        <Checkbox
          text="Hide Completed Tasks"
          defaultChecked={this.state.hideCompleted}
          onCheck={this.toggleHideCompleted}
          >
          </Checkbox>
        <label className="hide-completed">
          <input
            type="checkbox"
            readOnly={true}
            checked={this.state.hideCompleted}
            onClick={this.toggleHideCompleted}/>
          Hide Completed Tasks
        </label>

        <br/>

        <AccountsUIWrapper />

        { this.data.currentUser ?
          <form className="new-task" onSubmit={this.handleSubmit}>
            <input
              type="text"
              ref="textInput"
              placeholder="Type to add new tasks"/>
          </form> : ''
        }

        <Card>
          <List subheader="Aufgaben">
            {this.renderTasks()}
          </List>
        </Card>
      </div>
    );
  }
});